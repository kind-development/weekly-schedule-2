﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

using Xamarin.Forms;

namespace WeeklySchedule2
{
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
            carouselPg.CurrentPageChanged += CarouselPg_CurrentPageChanged;
            Debug.WriteLine("Event handler set up");
        }

        void CarouselPg_CurrentPageChanged(object sender, EventArgs e)
        {
            Debug.WriteLine("Current page changed event trigged");
            return;
        }

    }
}
